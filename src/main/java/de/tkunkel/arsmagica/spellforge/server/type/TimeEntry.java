package de.tkunkel.arsmagica.spellforge.server.type;

public class TimeEntry {

    private final Season season;
    private final int year;

    public TimeEntry(Season season, int year) {
        this.season = season;
        this.year = year;
    }

    public Season getSeason() {
        return season;
    }

    public int getYear() {
        return year;
    }

    public TimeEntry next() {

        Season newSeason = null;
        int newYear = year;

        switch (season) {

            case SPRING:
                newSeason = Season.SUMMER;
                break;
            case SUMMER:
                newSeason = Season.AUTUMN;
                break;
            case AUTUMN:
                newSeason = Season.WINTER;
                break;
            case WINTER:
                newSeason = Season.SPRING;
                newYear++;
                break;
        }

        return new TimeEntry(newSeason, newYear);
    }

    @Override
    public String toString() {
        return "TimeEntry{" +
                "season=" + season +
                ", year=" + year +
                '}';
    }
}
