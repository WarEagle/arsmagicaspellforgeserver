package de.tkunkel.arsmagica.spellforge.server.analyze;

import de.tkunkel.arsmagica.spellforge.server.type.Season;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Analysator {
    public static boolean contains(String text, int year, Season season) {
        Set<Pattern> patterns = new HashSet<>(season.toSearchStrings().size());

        if (text == null || text.isEmpty()) {
            return false;
        }

        for (String search : season.toSearchStrings()) {
            patterns.add(Pattern.compile(".*" + year + "\\s+" + search+".*"));
            patterns.add(Pattern.compile(".*" + search + "\\s+" + year+".*"));
        }

        boolean found = false;
        for (Pattern pattern : patterns) {
            Matcher matcher = pattern.matcher(text);
            if (matcher.matches()) {
                found = true;
                break;
            }
        }

        return found;
    }
}
