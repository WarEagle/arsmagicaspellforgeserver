package de.tkunkel.arsmagica.spellforge.server.type;

public class HistoryEntry {
    private final Season season;
    private final int year;
    private final String link;

    public HistoryEntry(Season season, int year, String link) {
        this.season = season;
        this.year = year;
        this.link = link;
    }

    public Season getSeason() {
        return season;
    }

    public int getYear() {
        return year;
    }

    @Override
    public String toString() {
        return "HistoryEntry{" +
                "season=" + season +
                ", year=" + year +
                ", link='" + link + '\'' +
                '}';
    }


}
