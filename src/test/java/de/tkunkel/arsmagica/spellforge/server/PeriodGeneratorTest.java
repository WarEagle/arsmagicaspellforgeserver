package de.tkunkel.arsmagica.spellforge.server;

import de.tkunkel.arsmagica.spellforge.server.type.Season;
import de.tkunkel.arsmagica.spellforge.server.type.TimeEntry;
import org.junit.Assert;
import org.junit.Test;

import java.util.Set;

import static org.junit.Assert.*;

public class PeriodGeneratorTest {

    @Test
    public void test(){
        TimeEntry from = new TimeEntry(Season.SPRING, 1000);
        TimeEntry to = new TimeEntry(Season.WINTER, 1000);
        Set<TimeEntry> list = PeriodGenerator.getList(from, to);

        Assert.assertEquals(4, list.size());
    }

}