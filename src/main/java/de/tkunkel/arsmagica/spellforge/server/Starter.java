package de.tkunkel.arsmagica.spellforge.server;

import de.tkunkel.arsmagica.spellforge.server.type.CharAnalyseResult;
import de.tkunkel.arsmagica.spellforge.server.analyze.CharPageExtractor;
import de.tkunkel.arsmagica.spellforge.server.type.Season;
import de.tkunkel.arsmagica.spellforge.server.type.TimeEntry;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Starter {

    private final Map<String, String> charUrls = new HashMap<>();

    public Starter() {
        charUrls.put("Taylor Read", "https://bsrp.tkunkel.de/w/index.php?title=TaylorRead");
        charUrls.put("Alasdair", "https://bsrp.tkunkel.de/w/index.php?title=Alasdair");
        charUrls.put("Earrach", "https://bsrp.tkunkel.de/w/index.php?title=Earrach");
        charUrls.put("Seraphine", "https://bsrp.tkunkel.de/w/index.php?title=Seraphine");
        charUrls.put("Coinneach", "https://bsrp.tkunkel.de/w/index.php?title=Coinneach");
    }

    public static void main(String[] args) throws IOException {

        Starter starter = new Starter();

        starter.start();
    }

    private void start() throws IOException {

        TimeEntry from = new TimeEntry(Season.SPRING, 1220);
        TimeEntry to = new TimeEntry(Season.SUMMER, 1222);
        Set<TimeEntry> times = PeriodGenerator.getList(from, to);

        CharAnalyseResult results = new CharAnalyseResult();
        for (Map.Entry<String, String> stringStringEntry : charUrls.entrySet()) {
            CharPageExtractor charPageExtractor = new CharPageExtractor(stringStringEntry.getValue());
            charPageExtractor.analyzeChar(results, stringStringEntry.getKey(), times);
        }

        System.out.println(results);
    }


}
