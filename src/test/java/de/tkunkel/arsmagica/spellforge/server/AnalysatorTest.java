package de.tkunkel.arsmagica.spellforge.server;

import de.tkunkel.arsmagica.spellforge.server.analyze.Analysator;
import de.tkunkel.arsmagica.spellforge.server.type.Season;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class AnalysatorTest {

    @Test
    public void emptyString() {
        String text = "";
        int year = 1220;
        Season season = Season.SPRING;
        boolean contains = Analysator.contains(text, year, season);

        Assert.assertFalse(contains);
    }


    @Test
    public void nullString() {
        String text = null;
        int year = 1220;
        Season season = Season.SPRING;
        boolean contains = Analysator.contains(text, year, season);

        Assert.assertFalse(contains);
    }


    @Test
    public void found() {
        String text = "Some Text containing Frühling 1220";
        int year = 1220;
        Season seson = Season.SPRING;
        boolean contains = Analysator.contains(text, year, seson);

        Assert.assertTrue(contains);
    }

    @Test
    public void notFound() {
        String text = "Some Text containing Winter 1220";
        int year = 1220;
        Season season = Season.SPRING;
        boolean contains = Analysator.contains(text, year, season);

        Assert.assertFalse(contains);
    }


    @Test
    public void notFound2() {
        String text = "Some Text containing Frühling and somewhere 1220, but seperated";
        int year = 1220;
        Season season = Season.SPRING;
        boolean contains = Analysator.contains(text, year, season);

        Assert.assertFalse(contains);
    }

}