package de.tkunkel.arsmagica.spellforge.server.type;

public enum CharAnalyseResultStatus {
    NOT_FOUND, ERROR, OK
}

