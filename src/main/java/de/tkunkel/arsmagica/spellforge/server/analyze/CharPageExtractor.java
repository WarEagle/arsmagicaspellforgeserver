package de.tkunkel.arsmagica.spellforge.server.analyze;

import de.tkunkel.arsmagica.spellforge.server.type.*;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.*;

public class CharPageExtractor {

    private final Document doc;

    public CharPageExtractor(String url) throws IOException {
        doc = Jsoup.connect(url).get();
    }

    public List<Element> extractHistoryEntries(Document doc) throws IOException {
        List<Element> rc = new ArrayList<>();

        Elements headlines = doc.select("#Entwicklung");
        for (Element headline : headlines) {
            rc.addAll(headline.parent().nextElementSibling().children());
        }

        Elements elements = doc.select("b");
        for (Element element : elements) {
            if (element.childNode(0).toString().startsWith("Entwicklung")) {
                rc.addAll(element.parent().nextElementSibling().children());
            }
        }

        return rc;
    }


    public List<HistoryEntry> analyseHistoryEntries(List<Element> history) {
        List<HistoryEntry> rc = new ArrayList<>();

        history.forEach(entry -> {
            rc.add(parse(entry));
        });

        return rc;
    }

    private HistoryEntry parse(Element entry) {
        HistoryEntry rc = null;

        Elements anchor = entry.select("a");

        String link;

        for (Element element : anchor) {
            link = element.attr("href");

            String text = element.text();
            int space = text.indexOf(" ");
            Season season = Season.fromString(text.substring(0, space).trim());

            String yearString = text.substring(space).trim();
            yearString = yearString.replaceAll(":", "");
            yearString = yearString.trim();

            rc = new HistoryEntry(season, Integer.parseInt(yearString), link);
        }

        //System.out.println(rc);

        return rc;
    }

    public void analyzeChar(CharAnalyseResult results, String charname, Set<TimeEntry> times) throws IOException {
        results.addCharResult(charname, CharAnalyseResultStatus.OK, new HashSet<>());

//        System.out.println(charname);
        List<Element> history = extractHistoryEntries(doc);
        List<HistoryEntry> historyEntries = analyseHistoryEntries(history);

        for (TimeEntry timeEntry : times) {
            boolean found = false;
            for (HistoryEntry historyEntry : historyEntries) {
                if (historyEntry.getSeason() == timeEntry.getSeason() && historyEntry.getYear() == timeEntry.getYear()) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                results.addCharResult(charname, CharAnalyseResultStatus.NOT_FOUND, timeEntry);
                //System.out.println("Missing: " + timeEntry);
            }
        }
    }


}
