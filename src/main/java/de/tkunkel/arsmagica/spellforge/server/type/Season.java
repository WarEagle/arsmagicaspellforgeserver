package de.tkunkel.arsmagica.spellforge.server.type;

import java.util.HashSet;
import java.util.Set;

public enum Season {
    SPRING("Frühling", "Fruehling", "Frühjahr", "Fruehjahr", "Spring"), SUMMER("Sommer", "Summer"), AUTUMN("Herbst", "Autumn"), WINTER("Winter");

    private final Set<String> searchString = new HashSet<>();

    Season(String... searchString) {
        for (String s : searchString) {
            this.searchString.add(s.trim());
        }
    }

    public Set<String> toSearchStrings() {
        return searchString;
    }

    public static Season fromString(String name){
        for (Season season : Season.values()) {
            if (season.name().equalsIgnoreCase(name)){
                return season;
            }
            for (String s : season.searchString) {
                if (s.equalsIgnoreCase(name)){
                    return season;
                }
            }
        }

        return null;
    }
}
