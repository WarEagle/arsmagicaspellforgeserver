package de.tkunkel.arsmagica.spellforge.server;

import de.tkunkel.arsmagica.spellforge.server.type.TimeEntry;

import java.util.HashSet;
import java.util.Set;

public class PeriodGenerator {

    public static Set<TimeEntry> getList(TimeEntry from, TimeEntry to) {
        //TODO add check if from > to
        Set<TimeEntry> rc = new HashSet<>();

        TimeEntry current = from;

        while (current.getSeason() != to.getSeason() || current.getYear() != to.getYear()) {
            rc.add(current);
            current = current.next();
        }
        rc.add(current);

        return rc;
    }
}
