package de.tkunkel.arsmagica.spellforge.server.type;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class CharAnalyseResult {

    private final Map<String, Set<TimeEntry>> results = new HashMap<>();

    public void addCharResult(String charname, CharAnalyseResultStatus status, TimeEntry missing) {
        this.results.putIfAbsent(charname, new HashSet<>());
        this.results.get(charname).add(missing);
    }

    public void addCharResult(String charname, CharAnalyseResultStatus status, Set<TimeEntry> missing) {
        this.results.putIfAbsent(charname, new HashSet<>());
        this.results.get(charname).addAll(missing);
    }

    @Override
    public String toString() {
        return "CharAnalyseResult{" +
                "results=" + results +
                '}';
    }
}
